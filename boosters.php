<a name="boosters" />
<div class="content-row alternating-background">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-lg-8 mx-auto text-center">
                <h2 class="mt-5">Join the parent boosters!</h2>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis est delectus ex sequi molestias
                    esse incidunt illo dolor suscipit ad architecto quasi nihil distinctio similique, perferendis
                    tempora explicabo consequuntur deserunt provident placeat quam unde minima expedita laudantium.
                    Nihil obcaecati quam sequi consectetur tenetur molestiae magnam. Eos rerum in tenetur modi debitis
                    omnis corrupti molestias laborum! Nemo optio debitis eligendi nesciunt?</p>
                <form class="boosters-signup">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Name</span>
                        </div>
                        <input type="text" class="form-control" id="boosters-signup-name" aria-label="Name"
                            aria-describedby="inputGroup-sizing-default">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Title</span>
                        </div>
                        <input type="text" class="form-control" id="boosters-signup-title" aria-label="Title"
                            aria-describedby="inputGroup-sizing-default">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Contact Number</span>
                        </div>
                        <input type="tel" class="form-control" id="boosters-signup-number" aria-label="Contact Number"
                            aria-describedby="inputGroup-sizing-default">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Email</span>
                        </div>
                        <input type="email" class="form-control" id="boosters-signup-email" aria-label="Email"
                            aria-describedby="inputGroup-sizing-default">
                    </div>

                    <button class="btn btn-primary">Join</button>
                </form>
            </div>
        </div>
    </div>
</div>
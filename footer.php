<footer>
        <div class="container">

            <div class="row">
                <div class="col-md-6 footer-col1">
                    <h2>Footer Column 1</h2>
                    <a href="#6" class="footer-link">Footer Link</a>
                    <a href="#7" class="footer-link">Footer Link</a>
                    <a href="#8" class="footer-link">Footer Link</a>
                    <a href="#9" class="footer-link">Footer Link</a>
                    <a href="#0" class="footer-link">Footer Link</a>
                </div>
                <div class="col-md-6 footer-col2">
                    <h2>Footer Column 1</h2>
                    <a href="#1" class="footer-link">Footer Link</a>
                    <a href="#2" class="footer-link">Footer Link</a>
                    <a href="#3" class="footer-link">Footer Link</a>
                    <a href="#4" class="footer-link">Footer Link</a>
                    <a href="https://www.blogger.com" class="footer-link">Edit News</a>
                </div>
            </div>
            <div class="row">

                <div class="col-md-12 copyright">
                    <p>Copyright &copy;2020 Draco Development Group</p>
                </div>
            </div>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>

</html>
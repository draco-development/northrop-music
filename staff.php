<a name="staff" />
<div class="content-row alternating-background">
    <div class="container">
        <div class="row staff">
            <h1>Director</h1>
            <p>
                <img src="img/bobcat.jpg" alt="bobcat" />
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium expedita eos nulla nobis
                molestias necessitatibus aliquam excepturi ex placeat. Sunt beatae sed reiciendis optio distinctio,
                nihil consequatur impedit est maxime possimus repellendus ipsam veritatis eum labore placeat eos
                neque quidem eligendi sapiente maiores dolor laudantium, saepe provident. Obcaecati ea atque eos
                necessitatibus minima. Alias, inventore. Aut, aspernatur deleniti voluptas ut quaerat animi, vel
                laudantium asperiores quasi, alias culpa cupiditate maiores sequi eius obcaecati saepe modi facere
                necessitatibus quibusdam dolorem! Sit.</p>
        </div>
        <div class="row staff right">
            <h1>Assistant</h1>
            <p>
                <img src="img/bobcat.jpg" alt="bobcat" />
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium expedita eos nulla nobis
                molestias necessitatibus aliquam excepturi ex placeat. Sunt beatae sed reiciendis optio distinctio,
                nihil consequatur impedit est maxime possimus repellendus ipsam veritatis eum labore placeat eos
                neque quidem eligendi sapiente maiores dolor laudantium, saepe provident. Obcaecati ea atque eos
                necessitatibus minima. Alias, inventore. Aut, aspernatur deleniti voluptas ut quaerat animi, vel
                laudantium asperiores quasi, alias culpa cupiditate maiores sequi eius obcaecati saepe modi facere
                necessitatibus quibusdam dolorem! Sit.</p>
        </div>
    </div>
</div>

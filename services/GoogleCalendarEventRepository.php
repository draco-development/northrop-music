<?php declare(strict_types=1);

class GoogleCalendarEventRepository implements ICalendarEventRepository {

    function getEvents(int $startDate, int $endDate): iterable {

        // this page uses php-restclient installed with composer.  run the following to restore the vendor files:
        //  php composer.phar install
        // php-restclient requires php7.2-curl php extension:
        //  sudo apt-get install php7.2-curl
        //  sudo service apache2 restart
    
        require_once('vendor/tcdent/php-restclient/restclient.php');
    
        $api = new RestClient([
            'base_url' => "https://www.googleapis.com/calendar/v3/calendars/f0flo3svkqs624249koe9h5mlk@group.calendar.google.com"  // <===== your calendar id here
        ]);
        $result = $api->get("events", 
            [
                'key' => "AIzaSyAp329eZiYBoakGqscFCXH4zc4VgGvLs9I",  //  <==== your API key here
                'timeMin' => "2020-01-26T00:00:00-05:00",
                'timeMax' => "2020-02-29T11:59:59-05:00",
                'showDeleted' => "false",
                'singleEvents' => "true"
            ]); 
        if($result->info->http_code == 200) {
            $response = $result->decode_response();
            $items = $response->items;
    
            foreach ($items as $item) {
                yield new CalendarEvent(
                    $item->htmlLink, $item->summary,
                    $item->start->date ?? '', $item->start->dateTime ?? '', 
                    $item->end->date ?? '', $item->end->dateTime ?? ''
                );
            }
        }
    }

}

// {
//     "htmlLink": "https://www.google.com/calendar/event?eid=NWdwZDQ3MjB1bmc2bGhxdTE3MmxjMmVsOWUgZjBmbG8zc3ZrcXM2MjQyNDlrb2U5aDVtbGtAZw",
//     "summary": "2day end of month test",
//     "start": {
//      "date": "2020-02-29"
//     },
//     "end": {
//      "date": "2020-03-01"
//     },
//    },


// "start": {
//     "dateTime": "2020-02-05T09:00:00-05:00"
//    },
//    "end": {
//     "dateTime": "2020-02-05T10:00:00-05:00"
//    },

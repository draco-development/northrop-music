<?php declare(strict_types=1);

class CalendarEvent {
    public function __construct(
        string $htmlLink,
        string $summary,
        string $startDate,
        string $startDateTime,
        string $endDate,
        string $endDateTime
    ) {
        $this->htmlLink = $htmlLink;
        $this->summary = $summary;
        $this->startDate = $startDate;
        $this->startDateTime = $startDateTime;
        $this->endDate = $endDate;
        $this->endDateTime = $endDateTime;
    }

    public $htmlLink;
    public $summary;
    
    public $startDate;
    public $startDateTime;

    public $endDate;
    public $endDateTime;
}

interface ICalendarEventRepository {
    function getEvents(int $startDate, int $endDate): iterable;
}


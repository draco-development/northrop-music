<?php declare(strict_types=1);

class Calendar {
    private $year;
    private $month;
    private $calendarEventRepository;
    
    public $currentMonth;
    
    public $previousMonth;
    public $previousYear;

    public $nextMonth;
    public $nextYear;

    public function __construct(
        int $year, int $month,
        ICalendarEventRepository $repository) {

        $this->year = $year;
        $this->month = $month;
        $this->calendarEventRepository = $repository;

        $this->currentMonth = mktime(0, 0, 0, $month, 1, $year);
        $this->previousMonth = ($month - 1) % 12 == 0 ? 12 : ($month - 1) % 12;
        $this->previousYear = $month == 1 ? $year - 1 : $year;
        $this->nextMonth = $month % 12 + 1;
        $this->nextYear = $month == 12 ? $year + 1 : $year;
    }

    public function getWeeks(): iterable {
        $startDate = Calendar::getStartDate($this->year, $this->month);
        $endDate = strtotime(date("Y-m-t", mktime(0, 0, 0, $this->month, 1, $this->year)));
        $events = $this->calendarEventRepository->getEvents($startDate, $endDate);

        $currentDate = $startDate;
        while ($currentDate < $endDate) {
            yield new CalendarWeek($currentDate, $events);
            $currentDate = strtotime('+7 days', $currentDate);
        }
    }

    public function generate(): iterable {
        $startDate = Calendar::getStartDate($year, $month);

        $currentDate = $startDate;
        for ($i = 0; $i < 7; $i++) {
            yield (object) [
                'class' => 'date',
                'text' => date('j', $currentDate)
            ];
            $currentDate = strtotime('+1 days', $currentDate);
        }
        // return $this->calendarEventRepository->getEvents($year, $month);
    }

    public static function priorMonthDaysInFirstWeek(int $year, int $month): int {
        $theDate = "$year-$month-01";
        $theTime = strtotime($theDate);
        $dayOfWeek = date('N', $theTime);
        $dayOfWeekStartingSunday = $dayOfWeek % 7;  // shift stupid ISO day of week so Sunday is 0.
        return $dayOfWeekStartingSunday;
    }

    public static function getStartDate(int $year, int $month): int {
        $days = Calendar::priorMonthDaysInFirstWeek($year, $month);
        $interval = new DateInterval('P'.$days.'D');
        $startDate = (new DateTime("$year-$month-01"))->sub($interval);
        return $startDate->getTimestamp();
    }

    public static function getEndDate(int $year, int $month): int {
        return strtotime(date("Y-m-t", strtotime("$year-$month-01")));
    }
}

// calculate startDate
// getDaysInMonth(year, month)
// add week{startDate,endDate} until endDate >= lastDayOfMonth(year,month)
// getEvents(startDate, endDate)

// generate days-of-week
// for each week
//   for each day
//     generate dates
//     calc # of days before first event of the week and render non-event spans
//     get events starting on this day, calc grid-column span/count
//     calc # of days after last event of week and render non-event spans
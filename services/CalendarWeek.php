<?php declare(strict_types=1);

class CalendarWeek {
    private $startDate;
    private $events;
    public function __construct(int $startDate, $events) {
        $this->startDate = $startDate;
        $this->events = $events;
    }

    public function getDays(): iterable {
        $currentDate = $this->startDate;
        for ($i = 0; $i < 7; $i++) {
            yield date('j', $currentDate);
            $currentDate = strtotime('+1 days', $currentDate);
        }
    }

    public $eventCache;
    public function buildEventCache(): void {
        $this->eventCache = array();
        $weekEndDate = strtotime("+6 days", $this->startDate);

        foreach($this->events as $event) {
            $eventStart = strtotime(($event->startDate == "") ? $event->startDateTime : $event->startDate); // int
            $eventEnd = strtotime(($event->endDate == "") ? $event->endDateTime : $event->endDate);         // int

            //IMPORTANT: for "all-day" events, API returns startDate + 1 day for a single day event (start - end = 1)
            if (($eventStart >= $this->startDate && $eventStart <= $weekEndDate) || 
                ($eventEnd >= $this->startDate && $eventEnd <= $weekEndDate)) {
                array_push($this->eventCache, (object) [
                    'htmlLink' => $event->htmlLink,
                    'summary' => $event->summary,                   // string
                    'start' => date("Y-m-d", $eventStart),          // string
                    'end' => date("Y-m-d", $eventEnd),              // string
                    'dayOfWeek' => $this->getDayOfWeek($eventStart), // int
                    'lengthInDaysThisWeek' => $this->getLengthInDaysThisWeek($eventStart, $eventEnd, $weekEndDate) //
                ]);
            }
        }
    }

    public function getDayOfWeek(int $eventStart) {
        if ($eventStart < $this->startDate) return 1;
        return (date('N', $eventStart) % 7) + 1;
    }

    public function getLengthInDaysThisWeek(int $eventStart, int $eventEnd, int $weekEndDate) {
        $secondsPerDay = 86400;
        return max(1, (min($weekEndDate, $eventEnd) - max($this->startDate, $eventStart)) / $secondsPerDay);
    }

    public function getEvents(int $index): iterable {
        $this->buildEventCache();
        
        $days = array();
        foreach ($this->eventCache as $event) {
            if (!array_key_exists($event->dayOfWeek, $days)) {
                $days[$event->dayOfWeek] = array();
            }
            array_push($days[$event->dayOfWeek], $event);
        }

        $theDay = 1;
        while ($theDay < 8) {
            if (array_key_exists($theDay, $days) && array_key_exists($index, $days[$theDay])) {
                $theEvent = $days[$theDay][$index];
                $theDay += $theEvent->lengthInDaysThisWeek;
                yield $theEvent;
            } else {
                $theDay += 1;
                yield "n/a";
            }
        }

    }
}

<?php declare(strict_types=1);

class MockCalendarEventRepository implements ICalendarEventRepository {
    
    public function getEvents(int $startDate, int $endDate): iterable {
        return array(
            new CalendarEvent(
                "https://www.google.com/calendar/event?eid=NmlvNWxpNzNlaGJhM3JldXBtbmE4Mmo5dW4gZjBmbG8zc3ZrcXM2MjQyNDlrb2U5aDVtbGtAZw",
                "Test",
                "2020-01-27", "",
                "2020-01-28", ""
            ),
            new CalendarEvent(
                "https://www.google.com/calendar/event?eid=MzkydWN1bHM1Z2llcHRxdmZiamkwbDJmbjEgZjBmbG8zc3ZrcXM2MjQyNDlrb2U5aDVtbGtAZw",
                "test2",
                "2020-02-21", "",
                "2020-02-22", ""
            ),
            new CalendarEvent(
                "https://www.google.com/calendar/event?eid=M2MzZm10M2NjbjQ3dDkzaWh1c3Y0MjV0aGcgZjBmbG8zc3ZrcXM2MjQyNDlrb2U5aDVtbGtAZw",
                "Test3",
                "2020-02-21", "",
                "2020-02-22", ""
            ),
            new CalendarEvent(
                "https://www.google.com/calendar/event?eid=NzdidTg0MThsdmZlcjBuNjcyMmg5cnExcmsgZjBmbG8zc3ZrcXM2MjQyNDlrb2U5aDVtbGtAZw",
                "2 day test",
                "2020-02-12", "",
                "2020-02-14", ""
            ),
            new CalendarEvent(
                "https://www.google.com/calendar/event?eid=MjczcTRiZHZtbzc2aThzODhwYm11aTRoN2ggZjBmbG8zc3ZrcXM2MjQyNDlrb2U5aDVtbGtAZw",
                "2day weekend test",
                "2020-02-15", "",
                "2020-02-17", ""
            ),
            new CalendarEvent(
                "https://www.google.com/calendar/event?eid=NWdwZDQ3MjB1bmc2bGhxdTE3MmxjMmVsOWUgZjBmbG8zc3ZrcXM2MjQyNDlrb2U5aDVtbGtAZw",
                "2day end of month test",
                "2020-02-29", "",
                "2020-03-01", ""
            ),
            new CalendarEvent(
                "https://www.google.com/calendar/event?eid=NzNhcG5jaGVwajJ2Y2oxMTdraWJoNG00ZDggZjBmbG8zc3ZrcXM2MjQyNDlrb2U5aDVtbGtAZw",
                "2 day beginning of month test",
                "2020-01-26", "",
                "2020-01-27", ""
            ),
            new CalendarEvent(
                "https://www.google.com/calendar/event?eid=MzI1bzAybmc1NGM4YjQ3a3NyODBvM251bG4gZjBmbG8zc3ZrcXM2MjQyNDlrb2U5aDVtbGtAZw",
                "Test",
                "", "2020-02-05T09:00:00-05:00",
                "", "2020-02-05T10:00:00-05:00"
            ),
            new CalendarEvent(
                "https://www.google.com/calendar/event?eid=M2w2NmJmMmo4YjJpcjh1YjYyZWhoMG44OXQgZjBmbG8zc3ZrcXM2MjQyNDlrb2U5aDVtbGtAZw",
                "Test",
                "", "2020-02-21T10:00:00-05:00",
                "", "2020-02-21T10:30:00-05:00"
            ),
            new CalendarEvent(
                "https://www.google.com/calendar/event?eid=NWU1NmJ1NnU5bDZhdTE3dHV2MTg0anM4NGwgZjBmbG8zc3ZrcXM2MjQyNDlrb2U5aDVtbGtAZw",
                "Test4",
                "", "2020-02-21T12:00:00-05:00",
                "", "2020-02-21T12:30:00-05:00"
            ),
            new CalendarEvent(
                "https://www.google.com/calendar/event?eid=MDFpbmhmZTNwc3FpcGlzZTYyaGZjOWhpMjYgZjBmbG8zc3ZrcXM2MjQyNDlrb2U5aDVtbGtAZw",
                "test5",
                "", "2020-02-21T13:00:00-05:00",
                "", "2020-02-21T13:30:00-05:00"
            )           
        );
    }

}
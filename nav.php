<nav class="navbar navbar-expand-lg">
    <a class="navbar-brand" href="index.php">Northrop Music</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="index.php#ensembles">Ensembles <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php#staff">Staff</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php#calendar">Calendar</a>
            </li>
            <li class="nav-item">
                <a class="navbar-brand" href="index.php">Northrop Music</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php#boosters">Boosters</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php#alumni">Alumni</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="blog.php">News</a>
            </li>
        </ul>
    </div>
</nav>
<a name="calendar" />
<div class="content-row alternating-background">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-lg-8 mx-auto text-center">

                <h2 class="mt-5">Here's our events calendar!</h2>

                <style>
                    .responsiveCal {
                        position: relative;
                        padding-bottom: 75%;
                        height: 0;
                        overflow: hidden;
                    }

                    .responsiveCal iframe {
                        position: absolute;
                        top: 0;
                        left: 0;
                        width: 100%;
                        height: 100%;
                    }
                </style>
                <div class="responsiveCal">
                    <!-- <iframe
                        src="https://calendar.google.com/calendar/embed?showTitle=0&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;showTz=0&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=7033mnq22c98dlp34lo25bb7i8%40group.calendar.google.com&amp;color=%23B1440E&amp;ctz=America%2FNew_York"
                        style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe> -->
                    <iframe
                        src="https://calendar.google.com/calendar/embed?src=f0flo3svkqs624249koe9h5mlk%40group.calendar.google.com&ctz=America%2FNew_York"
                        style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-lg-8 mx-auto text-center">
                <h2 class="mt-5">Get updates in your Inbox!</h2>
                <form>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">@</span>
                        </div>
                        <input type="text" class="form-control" placeholder="email address..."
                            aria-label="Email Address" aria-describedby="basic-addon1">
                        <div class="input-group-append">
                            <button class="btn btn-primary">Subscribe</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="calendar col-md-10 col-lg-8 mx-auto text-center">
                <?php
                    require_once 'services/ICalendarEventRepository.php';
                    require_once 'services/MockCalendarEventRepository.php';
                    require_once 'services/Calendar.php';
                    require_once 'services/CalendarWeek.php';
                    $repo = new MockCalendarEventRepository();

                    $year = date("Y");
                    $month = date("n");
                    if (array_key_exists("year", $_GET)) { $year = intval($_GET["year"]); }
                    if (array_key_exists("month", $_GET)) { $month = intval($_GET["month"]); }

                    $calendar = new Calendar($year, $month, $repo);

                ?>
                <div>
                    <h3>
                    <?php echo "<a class='month-link' href='index.php?year={$calendar->previousYear}&month={$calendar->previousMonth}#calendar'>&lt;</a>" ?>
                    <?php echo(date("F Y", $calendar->currentMonth)); ?>
                    <?php echo "<a class='month-link' href='index.php?year={$calendar->nextYear}&month={$calendar->nextMonth}#calendar'>&gt;</a>" ?>
                    </h3>
                </div>
                <div class="seven-cols weekdays-row">
                    <span>Sun</span>
                    <span>Mon</span>
                    <span>Tue</span>
                    <span>Wed</span>
                    <span>Thu</span>
                    <span>Fri</span>
                    <span>Sat</span>
                </div>

                <?php
                    foreach($calendar->getWeeks() as $week) {
                        ?> <div class="dates-row seven-cols"> <?php
                        foreach($week->getDays() as $day) {
                            ?> <span class="date"><?php echo($day); ?></span> <?php
                        }
                        ?> </div> <?php

                        ?> <div class="events-row seven-cols"> <?php
                        for ($i = 0; $i < 4; $i++) {
                            foreach($week->getEvents($i) as $event) {
                                if (gettype($event) == 'string') {
                                    ?> <span>&nbsp;</span> <?php
                                } else {
                                    ?>
                                        <span class="event"
                                            style="grid-column: <?php echo($event->dayOfWeek); ?> / span <?php echo($event->lengthInDaysThisWeek); ?>;">
                                            <a href="<?php echo($event->htmlLink); ?>" target="_blank">
                                                <?php echo($event->summary); ?>
                                            </a>
                                        </span> 
                                    <?php
                                }
                            }
                        }
                        ?> </div> <?php
                    }
                ?>

                <div class="calendar-footer">
                    <a href="https://calendar.google.com/calendar/ical/f0flo3svkqs624249koe9h5mlk%40group.calendar.google.com/public/basic.ics">
                        sync with this calendar
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>



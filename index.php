<?php require("header.php"); ?>

    <?php require("hero.php"); ?>
    <main>
        <?php require("ensembles.php"); ?>
        <?php require("staff.php"); ?>
        <?php require("calendar.php"); ?>
        <?php require("boosters.php"); ?>
        <?php require("alumni.php"); ?>
        <div class="content-row alternating-background">
            <div class="container">
                <div class="row">
                    <h1>Main Content</h1>
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium expedita eos nulla nobis
                        molestias necessitatibus aliquam excepturi ex placeat. Sunt beatae sed reiciendis optio distinctio,
                        nihil consequatur impedit est maxime possimus repellendus ipsam veritatis eum labore placeat eos
                        neque quidem eligendi sapiente maiores dolor laudantium, saepe provident. Obcaecati ea atque eos
                        necessitatibus minima. Alias, inventore. Aut, aspernatur deleniti voluptas ut quaerat animi, vel
                        laudantium asperiores quasi, alias culpa cupiditate maiores sequi eius obcaecati saepe modi facere
                        necessitatibus quibusdam dolorem! Sit.</p>
                </div>
            </div>
        </div>
        <div class="content-row alternating-background">
            <div class="container">
                <div class="row">
                    <h1>Main Content</h1>
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium expedita eos nulla nobis
                        molestias necessitatibus aliquam excepturi ex placeat. Sunt beatae sed reiciendis optio distinctio,
                        nihil consequatur impedit est maxime possimus repellendus ipsam veritatis eum labore placeat eos
                        neque quidem eligendi sapiente maiores dolor laudantium, saepe provident. Obcaecati ea atque eos
                        necessitatibus minima. Alias, inventore. Aut, aspernatur deleniti voluptas ut quaerat animi, vel
                        laudantium asperiores quasi, alias culpa cupiditate maiores sequi eius obcaecati saepe modi facere
                        necessitatibus quibusdam dolorem! Sit.</p>
                </div>
            </div>
        </div>
    </main>

<?php require("footer.php");

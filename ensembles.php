<a name="ensembles" />
<div class="content-row alternating-background">
    <div class="container">
        <div class="row">
            <h1>Ensembles</h1>
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Accusantium expedita eos nulla nobis
                molestias necessitatibus aliquam excepturi ex placeat. Sunt beatae sed reiciendis optio distinctio,
                nihil consequatur impedit est maxime possimus repellendus ipsam veritatis eum labore placeat eos
                neque quidem eligendi sapiente maiores dolor laudantium, saepe provident. Obcaecati ea atque eos
                necessitatibus minima. Alias, inventore. Aut, aspernatur deleniti voluptas ut quaerat animi, vel
                laudantium asperiores quasi, alias culpa cupiditate maiores sequi eius obcaecati saepe modi facere
                necessitatibus quibusdam dolorem! Sit.</p>
            <div id="ensemble-carousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="img/bobcat.jpg" alt="Backup Band">
                        <div class="carousel-caption">
                            <h3>Backup Band</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Est, illum.</p>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <img src="img/bobcat.jpg" alt="Color Guard">
                        <div class="carousel-caption">
                            <h3>Color Guard</h3>
                            <p>Los Angeles is so much fun!</p>
                        </div>
                    </div>

                    <div class="carousel-item">
                        <img src="img/bobcat.jpg" alt="Concert Band">
                        <div class="carousel-caption">
                            <h3>Concert Band</h3>
                            <p>Los Angeles is so much fun!</p>
                        </div>
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>
</div>

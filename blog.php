<?php require("header.php"); ?>


<div class="nav-background">
    <div class="container">
        <?php require("nav.php"); ?>
    </div>    
</div>
<div class="content-row">
    <div class="container">

<?php
    // goto www.blogger.com, create a blog
    // copy the blogId from the URL
    // goto https://developers.google.com/blogger
    // click Using the API under API v3.0
    // scroll down and click the Get API key button
    // after created, paste the key below.

    // this page uses php-restclient installed with composer.  run the following to restore the vendor files:
    //  php composer.phar install
    // php-restclient requires php7.2-curl php extension:
    //  sudo apt-get install php7.2-curl
    //  sudo service apache2 restart

    require_once('vendor/tcdent/php-restclient/restclient.php');

    $api = new RestClient([
        'base_url' => "https://www.googleapis.com/blogger/v3/blogs/1466956858817947226"  // <===== your blog id here
    ]);
    $result = $api->get("posts", 
        ['key' => "AIzaSyAp329eZiYBoakGqscFCXH4zc4VgGvLs9I"]);  //  <==== your API key here
    if($result->info->http_code == 200) {
        $response = $result->decode_response();
        $items = $response->items;

        foreach ($items as $item) {
            ?>
            <div class="post">
            <h1><?php echo($item->title); ?></h1>
            <p>published <?php echo($item->updated); ?> by <?php echo($item->author->displayName) ?></p>
            <div><?php echo($item->content); ?></div>
            </div>
            <?php
        }
    }

?>

    </div>
</div>

<?php require("footer.php"); ?>
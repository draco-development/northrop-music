<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

final class GoogleCalendarEventRepositoryTest extends TestCase {

    public function testGetEventsReturnsData(): void {
        $calendar = new GoogleCalendarEventRepository();
        $events = $calendar->getEvents(2020, 2);
        $this->assertIsIterable($events);
        $this->assertContainsOnlyInstancesOf(CalendarEvent::class, $events);
    }
}
<?php
declare(strict_types=1);
error_reporting(E_ALL);
use PHPUnit\Framework\TestCase;

final class CalendarWeekTest extends TestCase {

    public function testGetDaysReturnsStrings(): void {
        $week = new CalendarWeek(strtotime('2020-01-26'), NULL);
        $days = $week->getDays();
        $this->assertIsIterable($days);
        $this->assertContainsOnly('string', $days);
    }

    public function testGetEventsExists(): void {
        $week = new CalendarWeek(strtotime('2020-01-26'), NULL);
        $events = $week->getEvents(1);
        $this->assertIsIterable($events);
    }

    // public function testBuildEventCache(): void {
    //     $repo = new MockCalendarEventRepository();
    //     $events = $repo->getEvents(strtotime('2020-01-26'), strtotime('2020-02-04'));
    //     $week = new CalendarWeek(strtotime('2020-01-26'), $events);
    //     $week->buildEventCache();
    // }

    public function testGetDayOfWeek(): void {
        $week = new CalendarWeek(strtotime('2020-01-26'), NULL);
        $this->assertEquals(1, $week->getDayOfWeek(strtotime('2020-01-26')));
        $this->assertEquals(2, $week->getDayOfWeek(strtotime('2020-01-27')));
        $this->assertEquals(3, $week->getDayOfWeek(strtotime('2020-01-28')));
        $this->assertEquals(4, $week->getDayOfWeek(strtotime('2020-01-29')));
        $this->assertEquals(5, $week->getDayOfWeek(strtotime('2020-01-30')));
        $this->assertEquals(6, $week->getDayOfWeek(strtotime('2020-01-31')));
        $this->assertEquals(7, $week->getDayOfWeek(strtotime('2020-02-01')));

        $week = new CalendarWeek(strtotime('2020-02-09'), NULL);
        $this->assertEquals(7, $week->getDayOfWeek(strtotime('2020-02-15')));
    }

    public function testOneDayEventMidWeek(): void {
        $events = [
            new CalendarEvent(
                "https://www.google.com/calendar/event?eid=MzI1bzAybmc1NGM4YjQ3a3NyODBvM251bG4gZjBmbG8zc3ZrcXM2MjQyNDlrb2U5aDVtbGtAZw",
                "Test",
                "", "2020-02-05T09:00:00-05:00",
                "", "2020-02-05T10:00:00-05:00"
            )
            ];
        $week = new CalendarWeek(strtotime('2020-01-26'), $events);
        $weekEvents = $week->getEvents(0);
        $this->assertIsIterable($weekEvents);
    }

    public function validateDate($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }

    // public function testGetEventsIndexZero_Jan26ReturnsTwoItems() {
    //     $repo = new MockCalendarEventRepository();
    //     $repoEvents = $repo->getEvents(strtotime('2020-01-26'), strtotime('2020-02-01'));
    //     $week = new CalendarWeek(strtotime('2020-01-26'), $repoEvents);
    //     $events = $week->getEvents(0);
    //     $this->assertIsIterable($events);

    //     $expectedSummaries = ['2 day beginning of month test', 'Test'];
    //     foreach($events as $event) {
    //         var_dump($event);
    //         $summary = $event->summary;
    //         if (in_array($event->summary, $expectedSummaries)) {
    //             $expectedSummaries = array_map(function ($i) use ($summary) {
    //                     return $i == $summary ? null : $i;
    //                 }, $expectedSummaries);
    //         } else {
    //             $this->assertTrue(false);
    //         }
    //     }

    //     $this->assertTrue(array_sum(array_map(function ($i) {
    //             return is_null($i) ? 0 : 1;
    //         }, $expectedSummaries)) == 0);
    // }

    // public function testGetEventsIndexOne_Jan26ReturnsZeroItems() {
    //     $repo = new MockCalendarEventRepository();
    //     $repoEvents = $repo->getEvents(strtotime('2020-01-26'), strtotime('2020-02-04'));
    //     $week = new CalendarWeek(strtotime('2020-01-26'), $repoEvents);
    //     $events = $week->getEvents(1);
    //     $this->assertIsIterable($events);

    //     $count = 0;
    //     foreach ($events as $event) {
    //         $count++;
    //     }
    //     $this->assertTrue($count == 0);
    // }

    public function testGetLengthInDaysThisWeek() {
        $week = new CalendarWeek(strtotime('2020-02-09'), NULL);
        $this->assertEquals(2, $week->getLengthInDaysThisWeek(
            mktime(0, 0, 0, 2, 12, 2020), // eventStart
            mktime(0, 0, 0, 2, 14, 2020), // eventEnd
            mktime(0, 0, 0, 2, 15, 2020) // weekEndDate
        ));

        $this->assertEquals(1, $week->getLengthInDaysThisWeek(
            mktime(0, 0, 0, 2, 15, 2020), // eventStart
            mktime(0, 0, 0, 2, 17, 2020), // eventEnd
            mktime(0, 0, 0, 2, 15, 2020) // weekEndDate
        ));
    }

    public function testGetEventsFeb9_Returns6Items() {
        $repo = new MockCalendarEventRepository();
        $repoEvents = $repo->getEvents(strtotime('2020-01-26'), strtotime('2020-02-29'));
        $week = new CalendarWeek(strtotime('2020-02-09'), $repoEvents);

        $count = 0;
        foreach ($week->getEvents(0) as $event) {
            $count++;
        }
        $this->assertEquals(6, $count);
    }
}
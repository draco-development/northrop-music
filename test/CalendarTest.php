<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

final class CalendarTest extends TestCase {

    public function testGetWeeksExists() {
        $repo = new MockCalendarEventRepository();
        $calendar = new Calendar(2020, 2, $repo);
        $weeks = $calendar->getWeeks();
        $this->assertIsIterable($weeks);
        $this->assertContainsOnlyInstancesOf(CalendarWeek::class, $weeks);
    }

    public function testDayOfWeek(): void {
        $dateToTest = '2020-02-09';
        $timeToTest = strtotime($dateToTest);
        $dayOfWeek = date('l', $timeToTest);
        $this->assertEquals('Sunday', $dayOfWeek);
    }

    public function testpriorMonthDaysInFirstWeek_Feb2020_ShouldReturn6(): void {
        $this->assertEquals(6, Calendar::priorMonthDaysInFirstWeek(2020, 2));
    }

    public function testpriorMonthDaysInFirstWeek_Dec2019_ShouldReturn0(): void {
        $this->assertEquals(0, Calendar::priorMonthDaysInFirstWeek(2019, 12));
    }

    public function testpriorMonthDaysInFirstWeek_Apr2020_ShouldReturn3(): void {
        $this->assertEquals(3, Calendar::priorMonthDaysInFirstWeek(2020, 4));
    }

    public function testGetStartDate_Feb2020_ShouldReturn_Jan26(): void {
        $this->assertEquals(strtotime("2020-1-26"), Calendar::getStartDate(2020, 2));
    }

    public function testGetStartDate_Dec2019_ShouldReturn_Dec1(): void {
        $this->assertEquals(strtotime("2019-12-1"), Calendar::getStartDate(2019, 12));
    }
    
    public function testGetEndDate_Feb2020_ShouldReturn_Feb29(): void {
        $this->assertEquals(strtotime("2020-2-29"), Calendar::getEndDate(2020, 2));
    }

    public function testGetEndDate_Dec2019_ShouldReturn_Dec31(): void {
        $this->assertEquals(strtotime("2019-12-31"), Calendar::getEndDate(2019, 12));
    }
}
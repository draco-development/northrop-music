# Developer Quickstart (Windows 10, WSL -- Ubuntu, LAMP)
- clone this repo
- open the Microsoft Store and install the Ubuntu 18.04 LTS app
- once installed, open the Ubuntu terminal and install apache:
```bash
    sudo apt-get update
    sudo apt-get install apache2
    sudo service apache2 start
```
- link your code folder as a subfolder of /var/www/html
```bash
    ln -s  /mnt/c/source/draco/northop  /var/www/html
```
- open your browser to the url:
```
    http://localhost/northrop/index.php
```

# Running PHPUnit Tests
- if you've added or removed any classes, update the composer autoload by running:
```bash
    composer dump-autoload
```
- start test in watch mode (phpunit-watcher):
```bash
    vendor/bin/phpunit-watcher watch --testdox test
```